
const myHeroesArray = ["Harry Potter", "Gendalf", "Bilbo", "Saruman", "Aragorn", "Luke Skywalker", "Yoda"];
const myHeroesFunction = (array) => {
    const list = document.createElement("ul");
    const listElements = array.map(elem => `<li>${elem}</li>`);
    list.innerHTML = listElements.join("");
    document.body.append(list);

};
myHeroesFunction(myHeroesArray);


    // DOM это древовидная структура документа, модель, html разметка, представленная в виде обьектов,
    // позволяющее обращаться к любому его элементу по тегу, классу или айди